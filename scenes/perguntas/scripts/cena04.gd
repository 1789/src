extends TextureRect

#option é um nó OptonButton 
#export (NodePath) var option_path
#onready var option = get_node(option_path)
onready var option = get_node("option")

var selected
var dados

func _ready():
	var f = File.new()
	
	print("cena04 inicializada")
	
	f.open("data", File.READ)
	dados = f.get_var()
	selected = dados["Partido"]

	option.add_item("Escolha uma Opção")
	option.add_separator()
	#option recebe as opções de itens
	if selected == "Jacobino":
		option.add_item("OPÇÃO A) Tal comitê é prejudicial à Paris!")
		option.add_item("OPÇÃO B) Não devemos tomar tais medidas, tão perigosas à Paris")
	elif selected == "Girondino":
		option.add_item("OPÇÃO A) Deve-se combater a desordem e a violência e o comitê é a uma de nossas alternativas!")
		option.add_item("OPÇÃO B) Devemos investigar aqueles responsáveis pelas violências na cidade de Paris!")
	elif selected == "Planície":
		option.add_item("OPÇÃO A) Devemos, sim, investigar os eventos de setembro e agosto, mas com ressalvas")
		option.add_item("OPÇÃO B) Deve-se ter cautela ao tomar tais medidas de averiguação")
	
	f.close()

func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu' retorna ao menu
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	#Quando next é pressionado 
	#name recebe o nome digitado pelo usuario
	#dados recebe name, a opção de partido selecionada 
	#e o status do andamento do jogo
	#avança para a próxima cena
	var name = dados["Nome"]
	var f = File.new()

	f.open("data", File.READ)
	dados = f.get_var()
	f.close()
	f.open("data", File.WRITE)
	dados = {"Nome":name, "Partido":selected, "status":"cena05"}
	f.store_var(dados)
	f.close()
	get_tree().change_scene("res://scenes/perguntas/cena05.tscn")
