extends TextureRect

#option é um nó OptonButton 
#export (NodePath) var option_path
#onready var option = get_node(option_path)
onready var option = get_node("option")

var selected

func _ready():
	
	print("cena02 inicializada")
	#opttion se conecta a @on_item_selected()
	#option recebe as opções de itens
	option.connect("item_selected", self, "on_item_selected")
	option.add_item("Escolha um partido")
	option.add_separator()
	option.add_item("Jacobino")
	option.add_item("Girondino")
	option.add_item("Planície")

func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu' retorna ao menu
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	#Quando next é pressionado 
	#name recebe o nome digitado pelo usuario
	#dados recebe name, a opção de partido selecionada 
	#e o status do andamento do jogo
	#avança para a próxima cena
	var name = get_node("name").get_text()
	var f = File.new()
	var dados

	f.open("data", File.WRITE)
	dados = {"Nome":name, "Partido":selected, "status":"cena03"}
	f.store_var(dados)
	f.close()
	get_tree().change_scene("res://scenes/perguntas/cena03.tscn")

func on_item_selected(id):
	#Selected recebe o texto da opçao selecionada
	selected = option.get_item_text(id)